//
//  ViewController.swift
//  BiometryLibrary
//
//  Created by Jose Alvaro Gutierrez Romero on 04/12/2021.
//  Copyright (c) 2021 Jose Alvaro Gutierrez Romero. All rights reserved.
//

import UIKit
import BiometryLibrary

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    var biometry: BiometryManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        biometry = BiometryManager()
        // Do any additional setup after loading the view, typically from a nib.
        if biometry?.biometryType == .faceID {
            button.setTitle("Use FaceID", for: .normal)
        } else if biometry?.biometryType == .touchID {
            button.setTitle("Use touchID", for: .normal)
        } else {
            button.isHidden = true
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickButton(_ button: UIButton) {
        biometry?.setUserCredentials(username: "userRandom1", password: "pass")
        biometry?.biometricAuthValidation() { (success, error) in
            if success {
                print("Correct")
            } else {
                print(error)
            }
        }
    }

}

