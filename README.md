# BiometryLibrary

[![CI Status](https://img.shields.io/travis/Jose Alvaro Gutierrez Romero/BiometryLibrary.svg?style=flat)](https://travis-ci.org/Jose Alvaro Gutierrez Romero/BiometryLibrary)
[![Version](https://img.shields.io/cocoapods/v/BiometryLibrary.svg?style=flat)](https://cocoapods.org/pods/BiometryLibrary)
[![License](https://img.shields.io/cocoapods/l/BiometryLibrary.svg?style=flat)](https://cocoapods.org/pods/BiometryLibrary)
[![Platform](https://img.shields.io/cocoapods/p/BiometryLibrary.svg?style=flat)](https://cocoapods.org/pods/BiometryLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BiometryLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BiometryLibrary'
```

## Author

Jose Alvaro Gutierrez Romero, jose.alvaro.gutierrez.romero@everis.com

## License

BiometryLibrary is available under the MIT license. See the LICENSE file for more info.
