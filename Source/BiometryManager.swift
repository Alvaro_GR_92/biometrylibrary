//
//  BiometryManager.swift
//
//  Created by Jose Alvaro Gutierrez Romero on 8/4/21.
//

import Foundation
import LocalAuthentication

struct User: Codable {
    var username: String!
    var password: String!
    var keyBiometry: String!
    
    enum CodingKeys: String, CodingKey {
        case username
        case password
        case keyBiometry
    }
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        username = try container.decode(String.self, forKey: .username)
        password = try container.decode(String.self, forKey: .password)
        keyBiometry = try container.decode(String.self, forKey: .keyBiometry)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(username, forKey: .username)
        try container.encode(password, forKey: .password)
        try container.encode(keyBiometry, forKey: .keyBiometry)
    }
}

public class BiometryManager {
    
    // MARK: variables
    private var userCredentials: User?
    private var context = LAContext()
    var error: NSError?
    private var newUser = false
    
    public var biometryType: LABiometryType {
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            if context.biometryType == .faceID {
                return .faceID
            } else if context.biometryType == .touchID {
                return .touchID
            }
        }
        print(error?.localizedDescription ?? "Can't evaluate policity")
        return .none
    }
    
    
    // MARK: constructor
    public init() {
        context.localizedReason = "Enter App's passcode"
    }
    
    // MARK: private functions
    
    /// Check if user has register previously
    /// - Returns: true if the system found user credentials in keychain
    private func checkExistingUserData() -> Bool {
        var item: AnyObject?
        let userEncode = try? JSONEncoder().encode(userCredentials)
        
        let query: [String: Any] = {
            [kSecClass as String: kSecClassGenericPassword,
             kSecAttrAccount as String: userCredentials?.username! as Any,
//             kSecValueData as String: userCredentials?.password!.data(using: .utf8) as Any,
             kSecValueData as String: userEncode as Any,
            kSecReturnAttributes as String: true,
            kSecReturnData as String: true]
        }()
        
        let status = SecItemCopyMatching(query as CFDictionary, &item)
        
        if status == errSecItemNotFound {
            // If user data doesn't exist, we save it
            newUser = true
            // Save current domainState with user credentials.
            userCredentials?.keyBiometry = biometryDomainStateToString()
            return false
        }
        
        if status == errSecSuccess {
            // Check if domainState has changed
            let currentDomain = self.biometryDomainStateToString()
            // Get domainState saved
            let data = String(decoding: item?["v_Data"] as! Data, as: UTF8.self)
            let userData = data.data(using: .utf8)!
            let userDecode = try? JSONDecoder().decode(User.self, from: userData)
            if userDecode?.keyBiometry == currentDomain {
                return true
            }
        }
        printOSStateError(status: status)
        return false
    }
    
    /// This function saves user credentials in keychain
    private func saveBiometricAuthentication(completion: @escaping(Bool, OSStatus) -> ()) {
        let userEncode = try? JSONEncoder().encode(userCredentials)
        let query: [String: Any] = {
            [kSecClass as String: kSecClassGenericPassword,
             kSecAttrAccount as String: userCredentials?.username! as Any,
             kSecValueData as String: userEncode as Any]
        }()
        
        var result: AnyObject?
        let status = SecItemAdd(query as CFDictionary, &result)
        guard status == errSecSuccess else {
            completion(false, status)
            return
        }
        completion(true, status)
    }
    
    /// This funcion updates context.evaluatedPolicyDomainState value storaged in UserDefaults
    private func updateDomainStateStatus() -> Bool {
        if let domainString = biometryDomainStateToString() {
            UserDefaults.standard.setValue(domainString, forUndefinedKey: "domainRegistered")
            return true
        }
        
        return false
    }
    
    private func printOSStateError(status: OSStatus) {
        if let error: String = SecCopyErrorMessageString(status, nil) as String? {
            print(error)
        }
    }
    
    /// This function returns the domainState saved in UserDefaults
    private func getLastBiometryDomainStateStoraged() -> String {
        guard let domainString = userCredentials?.keyBiometry else {
            return ""
        }
        return domainString
    }
    
    /// This functions returns context.evaluatedPolicyDomainState value in string format
    private func biometryDomainStateToString() -> String? {
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            if let domainState = context.evaluatedPolicyDomainState {
                let bData = domainState.base64EncodedData()
                if let decodedString = String(data: bData, encoding: .utf8) {
                    return decodedString
                }
            }
        }
        return nil
    }
    
    /// This function checks if context.evaluatedPolicyDomainState has been changed
    private func checkStatusNotChanged() -> Bool {
        if !newUser {
            let oldDomain = getLastBiometryDomainStateStoraged()
            let currentDomain = biometryDomainStateToString()
            
            return oldDomain == currentDomain
        }
        return true
    }
    
    // MARK: public functions
    
    /// This function set user credentials of the login inputs
    public func setUserCredentials(username: String, password: String) {
        userCredentials = User(username: username, password: password)
    }
    
    /// this function checks whether the user has been able to register using biometrics
    /// - Parameters:
    ///   - completion: The completion handler is called with a boolean flag and a message of error
    ///   - flag: true if user can be registered
    ///   - error: error message if user can't authenticate with biometry
    public func biometricAuthValidation(completion: @escaping(_ flag:Bool, _ error:String?) -> ()) {
        
        context = LAContext()
        
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {

            let reason = "Log in to your account"
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { [self] success, error in

                if success {
                    if checkExistingUserData() {
                        // User exist
                        completion(true, "User exist")
                    } else if checkStatusNotChanged() {
                        // Register new user
                        saveBiometricAuthentication() { (success, status) in
                            if success {
                                completion(true, "A new user has registered")
                            } else {
                                if let error: String = SecCopyErrorMessageString(status, nil) as String? {
                                    completion(false, error)
                                }
                            }
                        }
                    } else {
                        // Se ha detectado un cambio en el domain y hay que resetear los datos.
                        completion(false, "Detected change in biometry, please entry again your biometry parameters")
                    }
                } else {
                    completion(false, error?.localizedDescription ?? "Failed to authenticate")
                }
            }
        } else {
            completion(false, error?.localizedDescription ?? "Can't evaluate policy")
        }
    }
}
